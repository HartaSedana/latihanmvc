<?php

class User extends Controller {
    public function index($company = 'SMKN1')
    {   
        $data['title'] = 'Home';
        $data['name'] = 'Pradnyana';
        $data['company'] = $company;
        $data['mhs'] = $this->model('User_model')->getAllUser();
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer');
    }
}
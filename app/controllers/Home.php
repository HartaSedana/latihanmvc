<?php 

require_once('../app/core/Flasher.php');

class Home extends Controller{

    public function __construct()
    {
        if(!isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL . '/auth/login');
            exit;
        }
    }
    public function index($company = 'SMKN1')
    {   
        $data['title'] = 'Home';
        $data['name'] = 'Pradnyana';
        $data['company'] = $company;
        $data['mhs'] = $this->model('User_model')->getAllUser();
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer');
    }

    public function about($company = 'SMKN1')
    {
        $data['title'] = 'About';
        $data['name'] = 'Pradnyana';
        $data['company'] = $company;
        $this->view('templates/header', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }

    public function createUser($company = 'SMKN1') {
        $data['title'] = 'Tambah Data User';
        $data['name'] = 'Pradnyana';
        $data['company'] = $company;
        $this->view('templates/header', $data);
        $this->view('home/create');
        $this->view('templates/footer');
    }

    public function create() {
        if($this->model('User_model')->create($_POST) > 0) {
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: '.BASE_URL.'/Home');
            exit;
        } else {
            Flasher::setFlash('Gagal', 'ditambahlkan', 'danger');
            return header("Location:" .BASE_URL. 'home/create');
        }
    }

    public function editUser($id) {

        $data['title'] = 'Edit User';
        $data['data'] = $this->model('User_model')->getUserById($id);
        $this->view('templates/header', $data );
        $this->view('home/create', $data);
        $this->view('templates/footer');
    }

    public function ubah()
    {
        if ($this->model('Example_model')->ubah($_POST) > 0) {
            Flasher::setFlash('berhasil', 'diubah', 'success');
            header('Location: http://..........');
            exit;
        }
    }

    public function delete($id)
    {
        $result = $this->model('User_model')->deleteUser($id);
        if($result > 0) {
            Flasher::setFlash('User', 'Berhasil di hapus', 'success');
            return header("Location:" .BASE_URL. '/');
            exit;
        }else {
            Flasher::setFlash('', 'Gagal Hapus data', 'danger');
            return header("Location:" .BASE_URL. '/');
        }
    }
}
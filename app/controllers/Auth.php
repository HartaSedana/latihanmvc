<?php

class Auth extends Controller {
    public function register()
    {   
        if(isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL .'/');
        }
        $this->view('auth/register');
    }

    public function login()
    {   
        if(isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL .'/');
        }
        $this->view('auth/login');
    }
    public function registerPost()
    {   
        if(isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL . '/');
        }
        if($this->model('User_model')->register($_POST) > 0) {
            return header('Location: ' . BASE_URL . '/auth/login');
        } else {
            return header('Location: ' . BASE_URL . '/auth/register');
        }
    }

    public function loginPost()
    {   
        if(isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL . '/');
        }
        if($this->model('User_model')->login($_POST)) {
            return header('Location: ' . BASE_URL . '/');

        } else {
            return header('Location: ' . BASE_URL . '/auth/login');
        }
    }

    public function logout()
    {   
        if(!isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL . '/auth/login');
        }
        session_destroy();
        return header('Location: ' . BASE_URL . '/auth/logout');
    }
    

}
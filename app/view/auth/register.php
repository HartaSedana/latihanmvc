<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Log In |MVC</title>
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/bootstrap.min.css">
</head>
<body>
<div class="card m-auto shadow mt-4" style="width: 30%;">
        <main class="form-control">
            <div class="text-center mt-2">
                <img src="<?= BASE_URL ?>/assets/image/bootstrap-logo-shadow.png" alt="" width="100">
            </div>
            <form method="post" action="<?= BASE_URL ?>/auth/registerPost">
                <h3 class="text-center my-3">Register</h3>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingInput" placeholder="Username" name="username" required/>
                    <label for="floatingInput">Username</label>
                </div>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingInput" placeholder="First Name" name="first_name" required/>
                    <label for="floatingInput">First Name</label>
                </div>
                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingInput" placeholder="Last Name" name="last_name" required/>
                    <label for="floatingInput">Last Name</label>
                </div>
                <div class="form-floating">
                    <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com" name="email" required/>
                    <label for="floatingInput">Email address</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password" required min="8"/>
                    <label for="floatingPassword">Password</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control mb-3" id="floatingConfirm" placeholder="Konfirmasi Password" name="confirm_password" required min="8"/>
                    <label for="floatingConfirm">Konfirmasi Password</label>
                </div>
                <button class="w-100 btn btn-lg btn-primary mb-3" type="submit" name="submit">Register</button>
                <div class="mb-3 w-100 text-center">
                    <p class="d-inline">have a account? </p><a href="<?= BASE_URL ?>/auth/login">Login</a>
                </div>
            </form>
        </main>
    </div>
</body>
</html>
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <h4 class="mt-3">Tambah Data</h4>
    <div class="row">
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-group">
                        <div class="card-body">
                            <form method="POST" action="<?= isset($data['data']) ? BASE_URL . '/home/editUser' : BASE_URL . '/home/create' ?>">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" required placeholder="usename" name="username"  value="<?= isset($data['data']) ? $data['data']['username'] : '' ?> >
                                </div>
                                <div class="form-group">
                                    <label class="mt-2">Email</label>
                                    <input type="text" class="form-control" required placeholder="email" name="email" >
                                </div>
                                <div class="form-group">
                                    <label class="mt-2">First Name</label>
                                    <input type="text" class="form-control" required placeholder="fist name" name="first_name" >
                                </div>
                                <div class="form-group">
                                    <label class="mt-2">Last Name</label>
                                    <input type="text" class="form-control" required placeholder="last name" name="last_name" >
                                </div>
                                <div class="form-group">
                                    <label class="mt-2">Password</label>
                                    <input type="password" class="form-control" required placeholder="" name="password" >
                                </div>
                                <a href="<?=BASE_URL?>/home/index" class="btn btn-dark mt-3"><i class="fas fa-arrow-left"></i>Back</a>
                                <button type="submit" class="btn btn-success mt-3">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>